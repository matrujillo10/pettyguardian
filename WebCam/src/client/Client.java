package client;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.imageio.stream.FileCacheImageInputStream;

public class Client {
	Socket socket;
	private InputStream in;
	
	public Client () throws UnknownHostException, IOException
	{
		socket = new Socket(InetAddress.getLocalHost(), 8080);
		in = socket.getInputStream();
	}
	
	public void rcv() throws IOException
	{
		File video = new File("Video.mjpeg");
		FileOutputStream fos = new FileOutputStream(video);
		byte[] data = new byte[1024];
		int count = in.read(data,0,1024);
		while (count != -1)
		{
			fos.write(data, 0, 1024);
			count = in.read(data,0,1024);
		}
		
	}
	
	public static void main(String[] args) throws UnknownHostException, IOException {
		Client cl = new Client();
	}

}
